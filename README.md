# mi web personal

No esta el código completo y no, no estoy orgulloso de este adefesio, tiene mil ideas malas pero es código muy viejo y sigue funcionando porque cumple su objetivo: dejarme hacer pruebas concentrándome en lo que estoy probando.

## [Zonas de Adams](http://jmdz.com.ar/apps/zonas-de-adams)
Es una prueba de [canvas](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API), [loadFromFile](https://gitlab.com/jmdz/fix-js), [highlight-external-links, nice-input-file, nice-link-up y varios fix-bs más](https://gitlab.com/jmdz/fix-bs).

## [Metáfora de la canilla](http://jmdz.com.ar/apps/metafora-de-la-canilla)
Mi primer juguete [SVG](https://developer.mozilla.org/en-US/docs/Web/SVG) a puro código `^_^`

## [Datos EXIF](http://jmdz.com.ar/apps/datos-exif)
Obviamente es el test de [js-metadata-jpeg](https://gitlab.com/jmdz/js-metadata-jpeg), pero también de [leaflet](https://leafletjs.com/). Que use [canvas](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API), [loadFromFile](https://gitlab.com/jmdz/fix-js) y otros ya da igual.

## [Recorrido GPS](http://jmdz.com.ar/apps/recorido-gps)
Por un lado quería seguir jugando con [leaflet](https://leafletjs.com/), por otro quería probar [bs_modal](https://gitlab.com/jmdz/fix-bs#nuevo-componente-bs_modal) pero además quería probar [chart.js](https://www.chartjs.org/) así que... acá va una primera versión de este experimento, pero aun esta muy cruda.

### TODO
* Agregar el curso.
* Agregar la actitud.
* Agregar el tiempo inmóvil.
* Esconder variables sin datos.
* Buscar las especificaciones de KML y GPX.
* Mejorar el gráfico:
	* Poner los nombres en los ejes *y* y sacar esa horrible referencia (o mejorar la referencia horrible).
	* Poner título al eje *x*.
	* Alternativas en el eje *x* a *Distancia recorrida*.
* Conseguir más archivos de prueba.
* Incluir los archivos de prueba en el repositorio.
* Armar un ejemplo inicial.
