<?php

require('../../apps/common.inc.php');

{$webpage
	->copy_year(2019)
	->title_segs('Zonas de Adams')
	->desc('Ansel Adams ademas de ser uno de los mejores fotógrafos también creó el sistema de zonas.')
	->keyw('Ansel, Adams, sistema, zonas')
	->inc_pkg('jquery-ui-1.12.1')
	->inc_pkg('jquery.ui.touch-punch-0.2.3')
;}

$webpage->head();

?>

<style>
#arrastrable{
	position:absolute;
	bottom:50px;
	left:calc(50% - 16.5vw + 0.25em);
	cursor:move;
	height:50px;
	width:calc(33vw - 0.5em);
	min-width:330px;
	padding:0;
	border:0 none;
}
#arrastrable div{
	display:inline-block;
	height:50px;
	width:9.09%;
	text-align:center;
	line-height:50px;
}
</style>

<? $webpage->body() ?>

<div class="row">

	<div class="col-12 col-sm-12">

		<p><a href="https://es.wikipedia.org/wiki/Ansel_Adams">Ansel Adams</a> ademas de ser uno de los mejores fotógrafos también creó el <a href="https://es.wikipedia.org/wiki/Sistema_de_zonas">sistema de zonas</a>.</p>

	</div>

</div>

<div class="row">

	<div class="col-12 col-sm-12 col-md-6 mt-3">

		<p>Este sistema consiste en tomar todos los valores tonales, dividirlos en once pasos, asignarle a cada paso un valor representativo y numerarlos desde cero hasta diez:</p>

		<table id="escala" class="table table-sm table-bordered text-center"></table>

		<table id="zonas" class="table table-sm table-bordered">
			<caption class="caption-top">Descripción de cada zona</caption>
			<thead class="thead-dark">
				<tr><th class="text-center">Zona</th><th class="text-center">Descripción</th></tr>
			</thead>
			<tbody>
				<tr><td class="text-center">0</td><td>Negro puro.</td></tr>
				<tr><td class="text-center">1</td><td>Negro sin textura.</td></tr>
				<tr><td class="text-center">2</td><td>Negro con textura.</td></tr>
				<tr><td class="text-center">3</td><td>Gris oscuro con poca textura.</td></tr>
				<tr><td class="text-center">4</td><td>Gris oscuro con textura: follaje oscuro, piedra oscura o sombras de paisaje.</td></tr>
				<tr><td class="text-center">5</td><td>Gris medio: piel oscura.</td></tr>
				<tr><td class="text-center">6</td><td>Gris claro: piel caucásica; sombras sobre la nieve en paisajes soleados.</td></tr>
				<tr><td class="text-center">7</td><td>Gris claro: piel muy clara; sombras en la nieve con iluminación lateral aguda.</td></tr>
				<tr><td class="text-center">8</td><td>Blanco con textura (textura de nieve).</td></tr>
				<tr><td class="text-center">9</td><td>Blanco sin textura (nieve flagrante).</td></tr>
				<tr><td class="text-center">10</td><td>Blanco puro (fuentes de luz).</td></tr>
			</tbody>
		</table>

	</div>

	<div class="col-12 col-sm-12 col-md-6 mt-3">

		<div class="form-group">
			<input
				data-nice-input-file="
					| class = like-no-readonly cursor-hand
					| placeholder = Elige una fotografía (JPEG)
					| button_text = Seleccionar
					| button_class = btn-primary
				"
				accept=".jpg,.jpeg,.jpe,.jif,.jfif,.jfi,image/jpeg"
				autocomplete="off"
				id="archivo"
				type="file"
			/>
			<small class="form-text text-success">
				Tu fotografía será analizada en tu propio dispositivo sin enviarse a ningún servidor.
			</small>
			<small class="form-text text-danger">
				No hay limite de tamaño pero si eliges un archivo de 50 <abbr title="Mebibyte" lang="en">MiB</abbr> va a ser leeeeeeennnnnnnttooooo.
			</small>
			<small class="form-text text-info">
				Puedes arrastrar la escala superpuesta que aparecerá.
			</small>
		</div>

		<div class="form-group text-center">
			<img id="imagen" class="mw-100 max-vh-100 py-3" />
		</div>

		<div id="arrastrable" class="d-none ui-widget-content"></div>

		<img
			class="d-none"
			crossorigin
			src="https://raw.githubusercontent.com/ianare/exif-samples/master/jpg/tests/28-hex_value.jpg"
			onload="procesar(this)"
		/>

	</div>

</div>

<script defer>

function procesar($img){
	setTimeout(
		function(){

			let canvas=document.createElement('canvas');
			canvas.width=$img.naturalWidth;
			canvas.height=$img.naturalHeight;

			let context=canvas.getContext('2d');
			context.drawImage($img,0,0);

			let pixels=context.getImageData(0,0,$img.naturalWidth,$img.naturalHeight);

			for(let i=0;i<pixels.data.length;i+=4){
				let avg=(pixels.data[i]+pixels.data[i+1]+pixels.data[i+2])/3;
				let zone=Math.floor(avg/256*11);
				let gray=255*zone*10/100;
				pixels.data[i]=gray;
				pixels.data[i+1]=gray;
				pixels.data[i+2]=gray;
			}

			context.putImageData(pixels,0,0,0,0,$img.naturalWidth,$img.naturalHeight);

			$('#imagen').attr('src',canvas.toDataURL());

			$('#archivo').prop('disabled',false);

			$( "#arrastrable" ).removeClass('d-none');

		}
		,500
	);
}

$(document).ready(function(){

	$('#archivo').on('click',function(){

		$('#imagen').attr('src','');

		$( "#arrastrable" ).addClass('d-none');

	});

	$('#archivo').on('change',function(){

		$('#archivo').toggleProp('disabled');

		$('<img/>').get(0).loadFromFile(
			$('#archivo').get(0)
			,function($file,$img/*=this*/){
				procesar($img);
			}
		);

	});

	{//arrastrable

		let arrastrable=$('#arrastrable');

		for(let i=0,d;i<11;i++){
			d=$('<div></div>');
			d.html(i);
			d.css({
				color:`rgb(${100-i*10}%,${100-i*10}%,${100-i*10}%)`,
				background:`rgb(${i*10}%,${i*10}%,${i*10}%)`,
			});
			arrastrable.append(d);
		}

		arrastrable.draggable();

	}//arrastrable

	{//escala

		let escala=$('#escala');

		let f1=$('<tr></tr>');
		f1.css('background','linear-gradient(90deg,#000,#fff)');
		f1.appendTo(escala)
		let cu=$('<td></td>');
		cu.html('&nbsp;');
		cu.attr('colspan','11');
		cu.appendTo(f1);

		let f2=$('<tr></tr>').appendTo(escala);
		f2.css('background','linear-gradient(90deg,#000,#fff)');
		for(let i=0,c,d;i<11;i++){
			c=$('<td></td>');
			c.html('&nbsp;');
			c.appendTo(f2);
		}

		let f3=$('<tr></tr>').appendTo(escala);
		for(let i=0,c,d;i<11;i++){
			c=$('<td></td>');
			c.html('&nbsp;');
			c.css('background',`rgb(${i*10}%,${i*10}%,${i*10}%)`);
			c.appendTo(f3);
		}

		let f4=$('<tr></tr>').appendTo(escala);
		for(let i=0,c,d;i<11;i++){
			c=$('<td></td>');
			c.html(i);
			c.css({
				color:`rgb(${100-i*10}%,${100-i*10}%,${100-i*10}%)`,
				background:`rgb(${i*10}%,${i*10}%,${i*10}%)`,
				width:'9.09%'
			});
			c.appendTo(f4);
		}

	}//escala

	{//zonas
		$('#zonas>tbody>tr>td:first-child').each(function($i,$e){
			$($e).css({
				color:`rgb(${100-$i*10}%,${100-$i*10}%,${100-$i*10}%)`,
				background:`rgb(${$i*10}%,${$i*10}%,${$i*10}%)`,
			});
		});
	}//zonas

});

</script>
