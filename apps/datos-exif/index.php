<?

require('../../apps/common.inc.php');

{$webpage
	->copy_year(2019)
	->title_segs('Datos EXIF')
	->desc('Todos los archivos guardan metadatos y los de las fotografías son muy útiles para los fotógrafos. Este es un lector de metadatos metadatos EXIF.')
	->keyw('metadatos, EXIF, fotografiás')
	->inc_pkg('jmdz-js-metadata-jpeg')
	->inc_pkg('leaflet-1.5.1')
	->inc_pkg('fontawesome-free-5.8.2')
;}

$webpage->head();

?>
<style>
span.rumbo{display:inline-block;width:1em;height:1em;line-height:1em;border-radius:0.5em;border:thin solid;text-align:center;}
dl[data-title]:empty{display:none;}
dl[data-title]::before{content:attr(data-title);font-size:1.5em;font-weight:bolder;border-bottom:thin solid;}
</style>
<? $webpage->body() ?>

<div class="row">
	<div class="col-12 col-sm-12 col-md-6 mt-3">
		<p>Todos los archivos guardan más datos que los que ves habitualmente. Estos son los <a href="https://es.wikipedia.org/wiki/Metadatos">metadatos</a>, la mayoría de las veces solo nos sirven a los informáticos pero los de las fotografías también son muy útiles para los fotógrafos. El principal estándar de metadatos para fotografiás es el <a href="https://es.wikipedia.org/wiki/Exchangeable_image_file_format"><abbr title="Exchangeable image file format" lang="en">EXIF</abbr></a>. Este es un lector de metadatos <abbr lang="en">EXIF</abbr>.</p>
	</div>
	<div class="col-12 col-sm-12 col-md-6 mt-3">
		<div class="form-group">
			<input
				data-nice-input-file="
					| placeholder = Elige la fotografía (JPEG)
					| button_text = Seleccionar
				"
				accept=".jpg,.jpeg,.jpe,.jif,.jfif,.jfi,image/jpeg"
				autocomplete="off"
				id="archivo"
				type="file"
			/>
			<small class="form-text text-success">
				Tu fotografía será analizada en tu propio dispositivo sin enviarse a ningún servidor.
			</small>
			<small class="form-text text-warning">
				La imagen se muestra escalada para tu pantalla y rotada/reflejada según la propiedad de orientación.
			</small>
			<small class="form-text text-danger">
				No hay limite de tamaño pero si eliges un archivo de 50 <abbr title="Mebibyte">MiB</abbr> puede ponerse leeeeeeennnnnnnttooooo.
			</small>
		</div>
	</div>
</div>

<div class="card-columns" id="salida"></div>

<template id="para_imagen">
	<div class="card w-100 card-body text-center">
		<img id="imagen" class="mw-100 max-vh-100" crossorigin/>
	</div>
</template>

<template id="para_lista">
	<dl id="[#lista#]" class="card w-100 card-body" data-title="[#titulo#]"></dl>
</template>

<template id="para_item">
	<dt>[#nombre#]</dt><dd>[#valor#]</dd>
</template>

<script defer>

function procesar(){
	lect.img.loadMetadata(function(metadata){
		if(
			lect.img.metadata.exif
			&&
			lect.img.metadata.exif.Orientation
		){
			lect.img.metadata.exif.Orientation.transform(
				$('#imagen').get(0)
			);
		}
		lect.show();
	});
}

$(document).ready(function(){

	window.lect={
		file:null
		,img:null
		,area:function($div_id,...$coord){
			setTimeout(
				function(){
					let nw,nh,aw,ah,mw,mh,zw,zh,mipa,canvas,context;
					nw=lect.img.naturalWidth;
					nh=lect.img.naturalHeight;
					aw=lect.img.width;
					ah=lect.img.height;
					mw=Math.ceil(aw/2);
					mh=Math.ceil(ah/2);
					zw=mw/nw;
					zh=mh/nh;
					mipa=new Image();
					mipa.src=lect.img.src;
					mipa.setAttribute('crossorigin','anonymous');
					mipa.onload=function(){
						mipa.onload=null;
						mipa.resizeTo(mw,mh);
						canvas=document.createElement('canvas');
						canvas.width=mw;
						canvas.height=mh;
						context=canvas.getContext('2d');
						context.drawImage(mipa,0,0,mw,mh);
						context.fillStyle='rgba(255,0,0,0.75)';
						switch($coord.length){
							case 2:{
								context.fillEllipse(
									Math.ceil(zw*$coord[0])
									,Math.ceil(zh*$coord[1])
									,9
									,9
									,0
									,0
									,2*Math.PI
								);
							}break;
							case 3:{
								context.fillEllipse(
									Math.ceil(zw*$coord[0])
									,Math.ceil(zh*$coord[1])
									,Math.ceil(zw*$coord[2]/2)
									,Math.ceil(zh*$coord[2]/2)
									,0
									,0
									,2*Math.PI
								);
							}break;
							case 4:{
								context.fillRect(
									Math.ceil(zw*($coord[0]-$coord[2]/2))
									,Math.ceil(zh*($coord[1]-$coord[3]/2))
									,Math.ceil(zw*$coord[2])
									,Math.ceil(zh*$coord[3])
								);
							}break;
						}
						mipa.src=canvas.toDataURL();
					};
					$('#'+$div_id).append(mipa);
				}
				,297
			);
		}
		,smap:function($div_id,$p_lat,$p_lng,$d_lat,$d_lng){

			const c=function($c){
				return eval($c.replace({
					Norte:'+1*(',
					Este:'+1*(',
					Sur:'-1*(',
					Oeste:'-1*(',
					'°':'+',
					'\'':'/60+',
					'\'\'':'/3600',
				})+'-0)');
			};

			let div=document.getElementById($div_id);
			div.className='mw-100 max-vh-100 py-3';
			div.style.height='400px';
			div.map=L.map($div_id);
			L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
				maxZoom:19,
				attribution:'&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
			}).addTo(div.map);

			let a=[];

			$p_lat=c($p_lat);
			$p_lng=c($p_lng);
			a.push([$p_lat,$p_lng]);
			L.marker([$p_lat,$p_lng],{icon:L.icon.glyph({prefix:'fas',glyph:'camera'})}).addTo(div.map);

			if($d_lat&&$d_lng){
				$d_lat=c($d_lat);
				$d_lng=c($d_lng);
				a.push([$d_lat,$d_lng]);
				L.marker([$d_lat,$d_lng],{icon:L.icon.glyph({prefix:'fas',glyph:'tree'})}).addTo(div.map);
			}

			div.map.fitBounds(a);

		}
		,show:function(){

			let metatags=HTMLImageElement.types.aliases['image/jpeg'].metatags;
			let props=metatags.map_exif._Props;

			let grupos=[
				{	clave:'file',titulo:'Datos básicos del archivo de imagen',
					propiedades:[
						{nombre:'Nombre'                                        ,valor:$b=>$b.name?$b.name:undefined},
						{nombre:'Formato'                                       ,valor:$b=>$b.type?$b.type:undefined},
						{nombre:'Tamaño'                                        ,valor:$b=>+$b.size?(+$b.size).toByteFormat(1)+' ('+$b.size+' B)':undefined},
						{nombre:'Ultima modificación'                           ,valor:$b=>$b.date?$b.date.getFullDateTime(' ',false):undefined},
						{nombre:'Dimensiones'                                   ,valor:$b=>$b.x&&$b.y?$b.x+' &times; '+$b.y+' px ('+($b.x*$b.y/1000000).toFixed(1)+' mpx)':undefined},
					],
				},
				{	clave:'exif',titulo:'Datos de fecha y hora',
					propiedades:[
						{nombre:'Ultima modificación'                           ,valor:$b=>$b.DateTime         &&!isNaN($b.DateTime.getTime()         )?$b.DateTime.getFullDateTime(' ',false)         +($b.SubSecTime         &&$b.SubSecTime.length<3         ?'.'+$b.SubSecTime         :'')+($b.OffsetTime         &&$b.OffsetTime!='   :  '         ?' '+$b.OffsetTime         :''):undefined},
						{nombre:'Creación'                                      ,valor:$b=>$b.DateTimeOriginal &&!isNaN($b.DateTimeOriginal.getTime() )?$b.DateTimeOriginal.getFullDateTime(' ',false) +($b.SubSecTimeOriginal &&$b.SubSecTimeOriginal.length<3 ?'.'+$b.SubSecTimeOriginal :'')+($b.OffsetTimeOriginal &&$b.OffsetTimeOriginal!='   :  ' ?' '+$b.OffsetTimeOriginal :''):undefined},
						{nombre:'Digitalización'                                ,valor:$b=>$b.DateTimeDigitized&&!isNaN($b.DateTimeDigitized.getTime())?$b.DateTimeDigitized.getFullDateTime(' ',false)+($b.SubSecTimeDigitized&&$b.SubSecTimeDigitized.length<3?'.'+$b.SubSecTimeDigitized:'')+($b.OffsetTimeDigitized&&$b.OffsetTimeDigitized!='   :  '?' '+$b.OffsetTimeDigitized:''):undefined},
						{nombre:'Del GPS'                                       ,valor:$b=>$b.GPSDateStamp||$b.GPSTimeStamp?($b.GPSDateStamp?$b.GPSDateStamp.replace(/:/g,'-'):'')+' '+($b.GPSTimeStamp?$b.GPSTimeStamp.map($v=>$v<10?'0'+$v:$v).join(':')+' (UTC)':''):undefined},
					],
				},
				{	clave:'exif',titulo:'Datos del usuario',
					propiedades:[
						'ImageDescription',
						'UserComment',
						'Artist',
						'CameraOwnerName',
						{nombre:'Copyright'                                     ,valor:$b=>$b.Copyright?$b.Copyright.replace(/\u0000+$/,'').replace(/\u0000/g,'<br/>'):undefined},
						'Software',
						'Make',
						'Model',
						'BodySerialNumber',
						'LensMake',
						'LensModel',
						'LensSerialNumber',
						{nombre:'Especificaciones del objetivo'                 ,valor:$b=>
							$b.LensSpecification
							?(
								$b.LensSpecification[0].valueOf()===$b.LensSpecification[1].valueOf()
								?
									'Distancia focal fija: '+$b.LensSpecification[0]+'mm'
									+'<br/>'
									+'Apertura máxima: '+(
										isNaN($b.LensSpecification[2])
										?'desconocida'
										:'f/'+$b.LensSpecification[2]
									)
								:
									'Distancia focal mínima: '+$b.LensSpecification[0]+'mm'
									+' (apertura máxima '+(
										isNaN($b.LensSpecification[2])
										?'desconocida'
										:'f/'+$b.LensSpecification[2]
									)+')'
									+'<br/>'
									+'Distancia focal máxima: '+$b.LensSpecification[1]+'mm'
									+' (apertura máxima '+(
										isNaN($b.LensSpecification[3])
										?'desconocida'
										:'f/'+$b.LensSpecification[3]
									)+')'
							)
							:undefined
						},
					],
				},
				{	clave:'exif',titulo:'Datos de la configuración del disparo',
					propiedades:[
						'ExposureProgram',
						'ExposureMode',
						{nombre:'Longitud focal'                                ,valor:$b=>$b.FocalLength?$b.FocalLength+'mm':undefined},
						{nombre:'Longitud focal (equivalente 35mm)'             ,valor:$b=>$b.FocalLengthIn35mmFilm?($b.FocalLengthIn35mmFilm?$b.FocalLengthIn35mmFilm+'mm':'desconocida'):undefined},
						'FNumber',
						'ApertureValue',
						'MaxApertureValue',
						{nombre:'Tiempo de exposición'                          ,valor:$b=>$b.ExposureTime?''+$b.ExposureTime.numerator+($b.ExposureTime.denominator==1?'':'/'+$b.ExposureTime.denominator)+'" ('+$b.ExposureTime+'s)':undefined},
						'ShutterSpeedValue',
						'ISOSpeedRatings',
						'PhotographicSensitivity',
						'SensitivityType',
						'StandardOutputSensitivity',
						'RecommendedExposureIndex',
						'ISOSpeed',
						'ISOSpeedLatitudeyyy',
						'ISOSpeedLatitudezzz',
						'BrightnessValue',
						'ExposureBiasValue',
						'ExposureIndex',
						'LightSource',
						{nombre:'Distancia del sujeto'                          ,valor:$b=>$b.SubjectDistance?$b.SubjectDistance+'m':undefined},
						'SubjectDistanceRange',
						{nombre:'Posición del sujeto'                           ,valor:$b=>
							(
								$b.SubjectArea
								||
								$b.SubjectLocation
							)
							?(
								'[#vals#]'
								+'<div id="[#id#]"></div>'
								+'<[#scr#]>lect.area("[#id#]",[#vals#])</[#scr#]>'
							).template({
								scr:'script',
								id:'AREA_'+Date.now(),
								vals:$b.SubjectArea?$b.SubjectArea:$b.SubjectLocation,
							})
							:undefined
						},
						'SceneType',
						'SceneCaptureType',
						'MeteringMode',
						'Flash',
						{nombre:'Potencia del flash'                            ,valor:$b=>$b.FlashEnergy?$b.FlashEnergy+'BCPS':undefined},
						'WhiteBalance',
						{nombre:'Zoom digital'                                  ,valor:$b=>$b.DigitalZoomRatio?($b.DigitalZoomRatio.numerator?$b.DigitalZoomRatio:'no usado'):undefined},
						'GainControl',
						'Contrast',
						'Saturation',
						'Sharpness',
						'SpectralSensitivity',
					],
				},
				{	clave:'exif',titulo:'Datos de la situación del disparo',
					propiedades:[
						{nombre:'Temperatura ambiental'                         ,valor:$b=>$b.Temperature?($b.Temperature.denominator==0xFFFFFFFF?'desconocida':$b.Temperature+'°C'):undefined},
						{nombre:'Humedad relativa ambiental'                    ,valor:$b=>$b.Humidity?($b.Humidity.denominator==0xFFFFFFFF?'desconocida':$b.Humidity+'%'):undefined},
						{nombre:'Presión atmosférica ambiental'                 ,valor:$b=>$b.Pressure?($b.Pressure.denominator==0xFFFFFFFF?'desconocida':$b.Pressure+'hPa'):undefined},
						{nombre:'Profundidad del agua'                          ,valor:$b=>$b.WaterDepth?($b.WaterDepth.denominator==0xFFFFFFFF?'desconocida':($b.WaterDepth<0?(-$b.WaterDepth)+'m (elevación)':$b.WaterDepth+'m')):undefined},
						{nombre:'Aceleración'                                   ,valor:$b=>$b.Acceleration?($b.Acceleration.denominator==0xFFFFFFFF?'desconocida':$b.Acceleration+'mGal'):undefined},
						{nombre:'Angulo de elevación de la camara'              ,valor:$b=>$b.CameraElevationAngle?($b.CameraElevationAngle.denominator==0xFFFFFFFF?'desconocido':$b.CameraElevationAngle+'°'):undefined},
						{nombre:'Dirección de la imágen'                        ,valor:$b=>
							$b.GPSImgDirection
							?(
								'<span class="rumbo" style="transform:rotate('+$b.GPSImgDirection+'deg);">&#8593;</span> '
								+$b.GPSImgDirection.toFixed(1)
								+' '
								+($b.GPSImgDirectionRef?$b.GPSImgDirectionRef:metatags[props.GPSImgDirectionRef._Last].GPS.GPSImgDirectionRef.T)
							)
							:undefined
						},
						{nombre:'Coordenadas de posición'                       ,valor:$b=>
							$b.GPSLatitudeRef&&$b.GPSLatitude&&$b.GPSLongitudeRef&&$b.GPSLongitude
							?`${$b.GPSLatitude}&nbsp;${$b.GPSLatitudeRef} ${$b.GPSLongitude}&nbsp;${$b.GPSLongitudeRef}`
							:undefined
						},
						{nombre:'Altitud'                                       ,valor:$b=>
							$b.GPSAltitude
							?(
								$b.GPSAltitude.toFixed(0)
								+'m '
								+($b.GPSAltitudeRef?$b.GPSAltitudeRef:metatags[props.GPSAltitudeRef._Last].GPS.GPSAltitudeRef[0])
							)
							:undefined
						},
						{nombre:'Velocidad'                                     ,valor:$b=>
							$b.GPSSpeed
							?(
								$b.GPSSpeed.toFixed(1)
								+' '
								+($b.GPSSpeedRef?$b.GPSSpeedRef:metatags[props.GPSSpeedRef._Last].GPS.GPSSpeedRef.K)
							)
							:undefined
						},
						{nombre:'Dirección de movimiento'                       ,valor:$b=>
							$b.GPSTrack
							?(
								'<span class="rumbo" style="transform:rotate('+$b.GPSTrack+'deg);">&#8593;</span> '
								+$b.GPSTrack.toFixed(1)
								+' '
								+($b.GPSTrackRef?$b.GPSTrackRef:metatags[props.GPSTrackRef._Last].GPS.GPSTrackRef.T)
							)
							:undefined
						},
						{nombre:'Coordenadas de destino'                        ,valor:$b=>
							$b.GPSDestLatitudeRef&&$b.GPSDestLatitude&&$b.GPSDestLongitudeRef&&$b.GPSDestLongitude
							?`${$b.GPSDestLatitude}&nbsp;${$b.GPSDestLatitudeRef} ${$b.GPSDestLongitude}&nbsp;${$b.GPSDestLongitudeRef}`
							:undefined
						},
						{nombre:'Distancia al destino'                          ,valor:$b=>
							$b.GPSDestDistance
							?(
								$b.GPSDestDistance.toFixed(1)
								+' '
								+($b.GPSDestDistanceRef?$b.GPSDestDistanceRef:metatags[props.GPSDestDistanceRef._Last].GPS.GPSDestDistanceRef.K)
							)
							:undefined
						},
						{nombre:'Dirección al destino'                          ,valor:$b=>
							$b.GPSDestBearing
							?(
								'<span class="rumbo" style="transform:rotate('+$b.GPSDestBearing+'deg);">&#8593;</span> '
								+$b.GPSDestBearing.toFixed(1)
								+' '
								+($b.GPSDestBearingRef?$b.GPSDestBearingRef:metatags[props.GPSDestBearingRef._Last].GPS.GPSDestBearingRef.T)
							)
							:undefined
						},
						{nombre:'Mapa'                                          ,valor:$b=>
							(
								$b.GPSLatitudeRef&&$b.GPSLatitude
								&&
								$b.GPSLongitudeRef&&$b.GPSLongitude
							)
							?(
								'<div id="[#id#]"></div>'
								+'<[#scr#]>lect.smap("[#id#]","[#p_lat#]","[#p_lng#]","[#d_lat#]","[#d_lng#]")</[#scr#]>'
							).template({
								scr:'script',
								id:'MAPA_'+Date.now(),
								p_lat:$b.GPSLatitudeRef+$b.GPSLatitude,
								p_lng:$b.GPSLongitudeRef+$b.GPSLongitude,
								d_lat:$b.GPSDestLatitudeRef&&$b.GPSDestLatitude?$b.GPSDestLatitudeRef+$b.GPSDestLatitude:'',
								d_lng:$b.GPSDestLongitudeRef&&$b.GPSDestLongitude?$b.GPSDestLongitudeRef+$b.GPSDestLongitude:'',
							})
							:undefined
						},
						'GPSSatellites',
						'GPSStatus',
						'GPSMeasureMode',
						'GPSDOP',
						'GPSMapDatum',
						'GPSProcessingMethod',
						'GPSAreaInformation',
						'GPSDifferential',
						'GPSHPositioningError',
					],
				},
				{	clave:'exif',titulo:'Datos de la estructura de la imagen',
					propiedades:[
						{nombre:'Dimensiones'                                   ,valor:$b=>
							$b.PixelXDimension&&$b.PixelYDimension
							?$b.PixelXDimension+' &times; '+$b.PixelYDimension+' px ('+($b.PixelXDimension*$b.PixelYDimension/1000000).toFixed(1)+' mpx)'
							:undefined
						},
						'Orientation',
						{nombre:'Resolución'                                    ,valor:$b=>
							$b.XResolution&&$b.YResolution&&$b.ResolutionUnit
							?$b.XResolution+' &times; '+$b.YResolution+' pixels por '+$b.ResolutionUnit
							:undefined
						},
						{nombre:'Resolución plano focal'                        ,valor:$b=>
							$b.FocalPlaneXResolution&&$b.FocalPlaneYResolution&&$b.FocalPlaneResolutionUnit
							?$b.FocalPlaneXResolution.toFixed(0)+' &times; '+$b.FocalPlaneYResolution.toFixed(0)+' pixels por '+$b.FocalPlaneResolutionUnit
							:undefined
						},
						'Compression',
						'ImageWidth',
						'ImageLength',
						'PhotometricInterpretation',
						'SamplesPerPixel',
						'BitsPerSample',
						'YCbCrSubSampling',
						'YCbCrPositioning',
						'YCbCrCoefficients',
						'PlanarConfiguration',
						'StripOffsets',
						'RowsPerStrip',
						'StripByteCounts',
						'JPEGInterchangeFormat',
						'JPEGInterchangeFormatLength',
						//TODO: 'TransferFunction',
						'WhitePoint',
						'PrimaryChromaticities',
						'ReferenceBlackWhite',
						'ColorSpace',
						'Gamma',
						'ComponentsConfiguration',
						'CompressedBitsPerPixel',
						//TODO: 'OECF',
						//TODO: 'SpatialFrequencyResponse',
						'SensingMethod',
						'FileSource',
						//TODO: 'CFAPattern',
						'CustomRendered',
						//TODO: 'DeviceSettingDescription',
					],
				},
				{	clave:'exif',titulo:'Otros datos almacenados',
					propiedades:[
						{nombre:'Miniatura'                                     ,valor:$b=>$b.thumbnail&&$b.thumbnail.image?$b.thumbnail.image.outerHTML:undefined},
						'ImageUniqueID',
						'ExifVersion',
						'FlashpixVersion',
						'GPSVersionID',
						'InteroperabilityIndex',
						'ExifIFDPointer',
						'GPSInfoIFDPointer',
						'InteroperabilityIFDPointer',
						'RelatedSoundFile',
						//TODO: 'MakerNote',
					],
				},
			];

			for(let grupo of grupos){
				if(lect.img.metadata&&lect.img.metadata[grupo.clave]){
					let il='LIST_'+Date.now();
					$('#salida').append(
						$('#para_lista').html().template({titulo:grupo.titulo,lista:il})
					);
					let dl=$('#'+il);
					for(let propiedad of grupo.propiedades){

						let nombre,valor;

						if(typeof(propiedad)==='string'){
							nombre=props[propiedad][props[propiedad]._Last].name;
							valor=lect.img.metadata[grupo.clave][propiedad];
						}
						else{
							nombre=propiedad.nombre;
							valor=propiedad.valor(lect.img.metadata[grupo.clave]);
						}

						if(isDefined(valor))dl.append($('#para_item').html().template({nombre:nombre,valor:valor}));

					}
				}
			}

		}
	};

	$('#archivo').on('click',function($event){
		lect.file=null;
		if(lect.img)lect.img.reset(true,true);
		lect.img=null;
		$('#salida').html('');
	});

	$('#archivo').on('change',function($event){
		if($('#archivo').get(0).files&&$('#archivo').get(0).files[0]){
			$('#salida').html($('#para_imagen').html());
			lect.file=$('#archivo').get(0).files[0];
			lect.img=$('#imagen').get(0);
			lect.img.loadFromFile(
				$('#archivo').get(0)
				,function(){
					procesar();
				}
			);
		}
	});

	$('#salida').html($('#para_imagen').html());
	$('#imagen').on('load',function($e){$('#imagen').off('load');lect.img=$('#imagen').get(0);procesar();});
	$('#imagen').attr('src','https://raw.githubusercontent.com/ianare/exif-samples/master/jpg/hdr/iphone_hdr_YES.jpg');

});

</script>
