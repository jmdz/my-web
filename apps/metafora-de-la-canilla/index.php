<?php

require('../../apps/common.inc.php');

{$webpage
	->copy_year(2019)
	->title_segs('Metáfora de la canilla')
	->desc('Probablemente la mejor forma de explicar el triangulo de exposición fotográfica es con la metáfora de la canilla.')
	->keyw('explicar, triangulo, exposición, fotográfica, metáfora, canilla')
;}

$webpage->body();

?>

<div class="row">
	<div class="col-12 col-sm-12">
		<p>Probablemente la mejor forma de explicar el triangulo de <a href="https://es.wikipedia.org/wiki/Exposición_(fotografía)">exposición fotográfica</a> es con la metáfora de la canilla. Tomar una foto es como llenar un balde de agua, un balde medio vacío es una foto sub-expuesta (empastada), un balde rebalsado es una foto sobre-expuesta (quemada). Aquí tienes un ejemplo muy limitado pero funcional.</p>
	</div>
</div>

<div class="row">
	<div class="col-12 col-sm-12 col-md-6 mt-3">

		<div class="form-group">
			<label for="apertura">Apertura</label>
			<p id="apertura_descripcion" class="form-text text-muted">
				La <a href="https://es.wikipedia.org/wiki/Apertura">apertura</a> (del <a href="https://es.wikipedia.org/wiki/Diafragma_(óptica)">diafragma</a> de la cámara) es como la la boca de la canilla, cuanto más grande sea más luz (agua) pasa. Se miden en <a href="https://es.wikipedia.org/wiki/Número_f_(óptica)">números "f"</a>, un número f grande es una apertura pequeña. Moviendo el deslizador cambias la boca de la canilla y a la derecha tiene el número f.
			</p>
			<div class="input-group">
				<input id="apertura" aria-describedby="apertura_descripcion" type="range" class="form-control" min="0" step="1" value="0" autocomplete="off"/>
				<div class="input-group-append">
					<span class="input-group-text" id="apertura_monitor"></span>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label for="sensibilidad">Sensibilidad</label>
			<p id="sensibilidad_descripcion" class="form-text text-muted">
				La <a href="https://es.wikipedia.org/wiki/Escala_de_sensibilidad_fotográfica">sensibilidad</a> (del <a href="https://es.wikipedia.org/wiki/Sensor_de_imagen">sensor</a> de la cámara) es como el tamaño del balde, cuanto menor sea más luz (agua) hará falta para llenarlo. Se mide en ISO, un ISO pequeño es una menor sensibilidad. Desplazando el deslizador cambias el tamaño del balde y derecha el indicador de ISO.
			</p>
			<div class="input-group">
				<input id="sensibilidad" aria-describedby="sensibilidad_descripcion" type="range" class="form-control" min="0" step="1" value="0" autocomplete="off"/>
				<div class="input-group-append">
					<span class="input-group-text" id="sensibilidad_monitor"></span>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label for="tiempo">Tiempo</label>
			<p id="tiempo_descripcion" class="form-text text-muted">
				El <a href="https://es.wikipedia.org/wiki/Tiempo_de_exposición">tiempo (de exposición)</a> es el tiempo que tenemos abierto el diafragma (o la canilla), cuanto más sea más luz (o agua) va a pasar. Se mide en segundos y fracciones de segundos, pero ojo que suele omitirse las comillas cuando son fracciones. Si mueves el deslizador veras que cambia el indicador pero el tiempo lo notaras cuando abras la canilla.
			</p>
			<div class="input-group">
				<input id="tiempo" aria-describedby="tiempo_descripcion" type="range" class="form-control" min="0" step="1" value="0" autocomplete="off"/>
				<div class="input-group-append">
					<span class="input-group-text" id="tiempo_monitor"></span>
				</div>
			</div>
		</div>

	</div>
	<div class="col-12 col-sm-12 col-md-6 mt-3 text-center">

		<div class="form-group">
			<button id="shoot" type="button" class="btn btn-primary">Disparar (abrir la canilla)</button>
		</div>

		<div class="form-group">
			<svg
				style="border:1px solid;width:400px;"
				xmlns="http://www.w3.org/2000/svg"
				version="1.1"
				viewBox="0 0 1600 1600"
				class="mw-100 max-vh-100"
			>
				<path
					id="canilla"
					data-v="|a:700|b:800"
					data-t="M 0 350 L 100 350 L 200 250 L 250 250 L 250 150 L 100 150 L 100 50 L 500 50 L 500 150 L 350 150 L 350 250 L 400 250 L 500 350 L 700 350 L {a} 350 L {b} 450 L {b} 650 L {a} 650 L 700 650 L 600 650 L 600 550 L 0 550 Z"
					fill="black"
				/>
				<path
					id="balde"
					data-v="|a:800"
					data-t="M 500 {a} L 500 1600 L 1500 1600 L 1500 {a}"
					fill="transparent"
					stroke="black" stroke-width="30"
				/>
				<path
					id="chorro"
					data-v="|a:800"
					data-t="M 600 650 L {a} 650 L {a} 1585 L 600 1585 Z"
					fill="transparent"
				/>
				<path
					id="agua"
					data-v="|a:800"
					data-t="M 515 {a} L 515 1585 L 1485 1585 L 1485 {a} Z"
					fill="transparent"
				/>
				<path
					id="raya"
					data-v="|a:785"
					data-t="M 485 {a} L 1515 {a}"
					fill="transparent"
					stroke="red" stroke-width="30" stroke-dasharray="30" stroke-dashoffset="10"
				/>
			</svg>
		</div>

	</div>
</div>

<script defer>

function draw($obj,$delta=0){
	let t=$obj.attr('data-t');
	let v=Object.fromListOne($obj.attr('data-v'));
	for(let p in v){
		v[p]=+v[p]+$delta;
	}
	$obj.attr('d',t.template(v,'{','}'));
}

function fill($obj,$color,$delay=0,$time=false){
	setTimeout(
		function(){
			let prev=$obj.attr('fill');
			$obj.attr('fill',$color);
			if($time!==false){
				setTimeout(
					function(){
						$obj.attr('fill',prev);
					}
					,$time
				);
			}
		}
		,$delay
	);
}

$(document).ready(function(){

	let apertura_valores=[
		{l:'f/8.0',d:0},
		{l:'f/5.6',d:200},
		{l:'f/4.0',d:600},
	];
	$('#apertura').attr('max',apertura_valores.length-1);
	$('#apertura').change(function(){
		$('#apertura_monitor').html(apertura_valores[$('#apertura').val()].l);
		draw($('#canilla'),apertura_valores[$('#apertura').val()].d);
		draw($('#chorro'),apertura_valores[$('#apertura').val()].d);
	});
	$('#apertura').change();

	let tiempo_valores=[
		{l:'1/2"',d:500*2},
		{l:'1"',d:1000*2},
		{l:'2"',d:2000*2},
	];
	$('#tiempo').attr('max',tiempo_valores.length-1);
	$('#tiempo').change(function(){
		$('#tiempo_monitor').html(tiempo_valores[$('#tiempo').val()].l);
	});
	$('#tiempo').change();

	let sensibilidad_valores=[
		{l:'ISO 100',d:0},
		{l:'ISO 200',d:400},
		{l:'ISO 400',d:600},
	];
	$('#sensibilidad').attr('max',sensibilidad_valores.length-1);
	$('#sensibilidad').change(function(){
		$('#sensibilidad_monitor').html(sensibilidad_valores[$('#sensibilidad').val()].l);
		draw($('#balde'),sensibilidad_valores[$('#sensibilidad').val()].d);
		draw($('#raya'),sensibilidad_valores[$('#sensibilidad').val()].d);
	});
	$('#sensibilidad').change();

	let exposicion_valores=[
		{d:750},
		{d:700},
		{d:600},
		{d:400},
		{d:0},
	];
	$('#shoot').click(function(){
		fill($('#agua'),'transparent',0,false);
		let e=+$('#apertura').val();
		e+=+$('#tiempo').val();
		draw($('#agua'),exposicion_valores[e].d);
		fill($('#chorro'),'blue',0,tiempo_valores[$('#tiempo').val()].d);
		fill($('#agua'),'blue',tiempo_valores[$('#tiempo').val()].d,false);
	});

});

</script>
