<header>
	<nav class="navbar navbar-dark bg-dark justify-content-start">
		<a class="navbar-brand" href="<?= $webpage->get_var('header-logo-link-href', $webpage->root())  ?>">
			<img alt="isotipo" class="align-text-bottom" src="<?= $webpage->stat('images/isotipo-tt-ww-t.png') ?>" style="width:2em;"/>
			<span class="h1">jMdZ</span>
		</a>
		<?php if ($webpage->get_var('header-subt-link-show', true)) { ?>
			<ul class="navbar-nav ">
				<li class="nav-item">
					<a class="nav-link" href="<?= $webpage->get_var('header-subt-link-href', '../') ?>">
						<span class="h2"><?= $webpage->get_var('header-subt-link-text', 'apps') ?></span>
					</a>
				</li>
			</ul>
		<?php } ?>
	</nav>
</header>

<section class="container-fluid">

<?php if ($webpage->get_var('header-ptit-show', true)) { ?>
	<div class="row">
		<div class="col-12 col-sm-12 mt-3">
			<h2><?= $webpage->title() ?></h2>
		</div>
	</div>
<?php } ?>
