<?php

require('../../apps/common.inc.php');

{$webpage
	->copy_year(2021)
	->title_segs('API Suma')
	->desc('Una API para testing')
	->keyw('api, test, testing')
;}

if (@ $_REQUEST['nums']) {
	$params = (object) $_REQUEST;

	$func = $params->func ?? '';

	$secs = (int) ($params->secs ?? 0);

	$code = (int) ($params->code ?? 200);

	if ($code < 200 || 599 < $code) $code = 200;

	$nums = array_map(
		function($n) {
			return (float) $n;
		},
		preg_split(
			'~,~',
			@ $params->nums ?? '',
			null,
			PREG_SPLIT_NO_EMPTY
		)
	);

	$sum = array_sum($nums);

	sleep($secs);

	$outp = compact(
		'func',
		'secs',
		'code',
		'nums',
		'sum'
	);

	http_response_code($code);

	$webpage->http_head('Access-Control-Allow-Origin: *');

	if ($func) {
		$webpage->mime_type('application/javascript');
		$webpage->charset('UTF-8');
		$webpage->head();
		echo "$func(", json_encode($outp, JSON_PRETTY_PRINT), ");";
		exit();
	} else {
		$webpage->json($outp, JSON_PRETTY_PRINT);
	}
}

$webpage->head();

?>
<style>
</style>
<? $webpage->body() ?>

<div class="row">
	<div class="col-12 col-sm-12 col-md-12 mt-3">
		<p><strong>Esta es una API para hacer pruebas</strong></p>

		<p>Puedes llamar a esta misma URL con cualquier método</p>

		<p>Por donde sea tienes cuatro parámetros disponibles para usar:</p>

		<dl>
			<dt><code>nums</code></dt>
			<dd>Un listado de números separados por comas (<code>,</code>). El separador decimal es el punto (<code>.</code>).</dd>

			<dt><code>code</code></dt>
			<dd>El código de estado HTTP que deseas recibir. Ver <a href="https://en.wikipedia.org/wiki/List_of_HTTP_status_codes">List of HTTP status codes</a>.</dd>

			<dt><code>secs</code></dt>
			<dd>Una cantidad de segundos de demora extra.</dd>

			<dt><code>func</code></dt>
			<dd>El nombre de una función a invocar en la respuesta (<em>callback</em>). Ver <a href="https://en.wikipedia.org/wiki/JSONP">JSONP</a>.</dd>
		</dl>

		<p>El resultado es un objeto plano con los cuatro parámetros más una propiedad <code>sum</code> que es el resultado de la suma. Ver <a href="https://en.wikipedia.org/wiki/JSON">JSON</a>.</p>

		<p>Si llamas a esta url sin el parámetros <code>nums</code> veras esta página.</p>

		<h3>Ejemplos</h3>
		<ul>
			<li><a href="?nums=1,2,3"><?= $webpage->root('apps/api-suma',true, true) ?>?nums=1,2,3</a></li>
			<li><a href="?nums=1,2,3&code=403&secs=9"><?= $webpage->root('apps/api-suma',true, true) ?>?nums=1,2,3&code=403&secs=9</a></li>
			<li><a href="?nums=1,2,3&secs=9"><?= $webpage->root('apps/api-suma',true, true) ?>?nums=1,2,3&secs=9</a></li>
			<li><a href="?nums=1,2,3&func=myFunc"><?= $webpage->root('apps/api-suma',true, true) ?>?nums=1,2,3&func=myFunc</a></li>
		</ul>

	</div>
</div>

<script defer>
</script>
