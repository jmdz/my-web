<?php

require('../../webpage.inc.php');

{$webpage

	->html_attrs('class','h-100')
	->body_attrs('class','fix-bs h-100 d-flex flex-column highlight-external-links')

	->inc_pkg('jmdz-fix-js')
	->inc_pkg('jquery-3.4.1')
	->inc_pkg('popper-1.14.7')
	->inc_pkg('bootstrap-4.3.1')

	->inc_on_body('../../apps/header.inc.php')
	->inc_on_end( '../../apps/footer.inc.php')

;}
