<?php

error_reporting(-1);
ini_set('display_errors', 1);

require('../../apps/common.inc.php');

{$webpage
	->copy_year(2021)
	->title_segs('YouTube CSV to OPML')
	->desc('Conversor del CSV de suscripciones de YouTube a OPML.')
	->keyw('conversor, csv, suscripciones, youtube, opml')
;}

$webpage->body();

?>

<div class="row">
	<div class="col-12 col-sm-12 col-md-6">
		<p>Realmente hay mucho para mejorar en este aplicativo pero es un comienzo.</p>
		<div class="form-group">
			<input
				data-nice-input-file="
					| class = like-no-readonly cursor-hand
					| placeholder = Elige el archivo CSV
					| button_text = Seleccionar
					| button_class = btn-primary
				"
				accept=".csv"
				autocomplete="off"
				id="archivo"
				type="file"
			/>
			<small class="form-text text-success">
				Tu archivo será analizado en tu propio dispositivo sin enviarse a ningún servidor.
			</small>
		</div>

		<div>
			<table id="csv" class="max-vh-100 text-2 table table-sm table-responsive table-striped table-hover" hidden>
				<thead class="thead-dark"></thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
	<div class="col-12 col-sm-12 col-md-6">
		<div class="form-group">
			<textarea id="opml" rows="99" class="max-vh-100 form-control" readonly hidden></textarea>
		</div>

		<div class="form-group text-center">
			<button id="descargar" hidden class="btn btn-primary">Descargar OPML</button>
		</div>
	</div>
</div>


<script defer>

function csv2arrayOfObjects($csv){
	let aoo = $csv
		.trim()
		.split('\n')
		.filter(
			$r => $r.length
		)
		.map(
			$r => $r.split(',', 3)
		)
	;

	let t = aoo
		.shift()
		.map(
			$t => $t.replace(/\s/g, '_')
		)
	;

	aoo = aoo.map(
		$r => Object.fromEntries(
			t.map(
				($t, $i) => [$t, $r[$i]]
			)
		)
	);

	return aoo;
}

function arrayOfObjects2table($aoo, $jq_table){
	let t = Object.keys($aoo[0]);

	$jq_table.find('thead').html(''
		+ '<tr>'
		+ t.map( $t => `<th>${$t.replace(/_/g, ' ')}</th>` ).join('')
		+ '</tr>'
	);

	$jq_table.find('tbody').html(''
		+ $aoo
			.map(
				$o => ''
					+ '<tr>'
					+ Object.entries($o)
						.map( $p => `<td>${$p[1]}</td>` )
						.join('')
					+ '</tr>'
			)
			.join('')
	);

}

function arrayOfObjects2OPML($aoo){
	let opml = ''
		+ '<' + '?xml version="1.0"?' + '>\n'
		+ '<opml version="1.0">\n'
		+ '\t<head>\n'
		+ '\t\t<title>YouTube subcriptions</title>\n'
		+ '\t</head>\n'
		+ '\t<body>\n'
	;

	let t = Object.keys($aoo[0]);

	$aoo.forEach(
		$o => {
			opml += ''
				+ '\t\t<outline'
				+ ' text="' + $o[t[2]] /* .replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;') */ + '"'
				+ ' xmlUrl="https://www.youtube.com/feeds/videos.xml?channel_id=' + $o[t[0]] + '"'
				+ ' htmlUrl="' + $o[t[1]] + '"'
				+ ' type="rss"'
				+ '/>\n'
			;
		}
	);

	opml += ''
		+ '\t</body>\n'
		+ '</opml>\n'
	;

	return opml;
}

$(document).ready(function(){
	$('#descargar').on('click',function(){
		if(
			$('#archivo').get(0)
			&& $('#archivo').get(0).files
			&& $('#archivo').get(0).files[0]
			&& $('#archivo').get(0).files[0] instanceof File
		){
			var f = new Blob([$('#opml').val()], {type: 'text/plain'});

			let a = document.createElement("a");
			a.download = $('#archivo').get(0).files[0].name.replace(/\.[^.]+$/, '.opml');
			a.href = window.URL.createObjectURL(f);
			a.click();
		}
	});

	$('#archivo').on('change',function(){
		if(
			$('#archivo').get(0)
			&& $('#archivo').get(0).files
			&& $('#archivo').get(0).files[0]
			&& $('#archivo').get(0).files[0] instanceof File
		){
			$('#archivo').toggleProp('disabled');
			$('#csv').prop('hidden', true);
			$('#opml').prop('hidden', true);
			$('#descargar').prop('hidden', true);

			let reader=new FileReader();

			reader.onload=function($e){
				let datos = csv2arrayOfObjects($e.target.result);

				arrayOfObjects2table(datos, $('#csv'));

				$('#opml').val(arrayOfObjects2OPML(datos));

				$('#archivo').toggleProp('disabled');
				$('#csv').toggleProp('hidden');
				$('#opml').toggleProp('hidden');
				$('#descargar').toggleProp('hidden');
			};

			reader.readAsText($('#archivo').get(0).files[0]);
		}
	});
});

</script>
