<?

require('../webpage.inc.php');

{$webpage

	->title_segs('Apps')
	->copy_year(2019)

	->html_attrs('class','h-100')
	->body_attrs('class','fix-bs h-100 d-flex flex-column highlight-external-links')

	->set_var('header-subt-link-show', false)
	->inc_on_body( '../apps/header.inc.php')
	->inc_on_end( '../apps/footer.inc.php')

	->inc_pkg('jmdz-fix-js')
	->inc_pkg('jquery-3.4.1')
	->inc_pkg('popper-1.14.7')
	->inc_pkg('bootstrap-4.3.1')

;}

$apps=glob('*',GLOB_ONLYDIR);
if($webpage->prod()){
	$apps=array_filter(
		$apps
		,function($v){
			return $v[0]!=='-';
		}
	);
}
$apps=array_map(
	function($v){
		return (object)[
			'url'=>$v
			,'dev'=>$v[0]==='-'
			,'title'=>ucwords(trim(str_replace('-',' ',$v)))
		];
	}
	,$apps
);

$webpage->body();

?>

<div class="row">
	<div class="col-12 mt-3">
		<ul>
<?foreach($apps as $a){?>
			<li><a class="<?=$a->dev?'text-danger':''?>" href="./<?=$a->url?>"><?=$a->title?></a></li>
<?}unset($a)?>
		</ul>
	</div>
</div>
