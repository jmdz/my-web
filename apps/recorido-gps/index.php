<?php

require('../../apps/common.inc.php');

{$webpage
	->copy_year(2019)
	->title_segs('Recorrido GPS')
	->desc('Analizador de archivos GPX y KML.')
	->keyw('analizador, GPS, GPX, KML')
	->inc_pkg('leaflet-1.5.1')
	->inc_pkg('chart.js-2.8.0')
;}

$webpage->body();

?>

<div class="row">
	<div class="col-12 col-sm-12 col-md-6">
		<div id="c2" class="">
			<p>Si usas un GPS y sueles guardas tus paseos en GPX o KML aquí tienes un analizador de esos archivos.</p>
			<p>Realmente hay mucho para mejorar en este aplicativo pero es un comienzo.</p>
			<div class="form-group">
				<input
					data-nice-input-file="
						| class = like-no-readonly cursor-hand
						| placeholder = Elige el archivo GPX o KML
						| button_text = Seleccionar
						| button_class = btn-primary
					"
					accept=".gpx,.kml"
					autocomplete="off"
					id="archivo"
					type="file"
				/>
				<small class="form-text text-success">
					Tu archivo será analizado en tu propio dispositivo sin enviarse a ningún servidor.
				</small>
			</div>
		</div>
		<div id="c3" class=""></div>
	</div>
	<div class="col-12 col-sm-12 col-md-6">
		<div id="c1" class=""></div>
		<div id="c4" class=""></div>
	</div>
</div>

<template id="t1">
	<div class="mw-100 max-vh-100 py-3" style="height:400px;" id="[#id#]"></div>
</template>

<template id="t3">
	<table class="table table-striped table-bordered table-hover">
		<caption class="caption-top"></caption>
		<thead class="thead-dark ">
			<tr>
				<th></th>
				<th scope="col">Inicio</th>
				<th scope="col">Variación</th>
				<th scope="col">Fin</th>
			</tr>
		</thead>
		<tbody>
<!--
			<tr>
				<th scope="row">Tipo de archivo</th>
				<td colspan="3">[#type#]</td>
			</tr>
			<tr>
				<th scope="row">Cantidad de puntos de registro</th>
				<td colspan="3">[#length#]</td>
			</tr>
-->
			<tr>
				<th scope="row">Fecha y hora</th>
				<td>[#startDateTimeString#]</td>
				<td>[#durationString#] ([#durationExactString#])</td>
				<td>[#endDateTimeString#]</td>
			</tr>
			<tr>
				<th scope="row">Coordenadas</th>
				<td>[#startCoordsString#]</td>
				<td>[#distanceString#]</td>
				<td>[#endCoordsString#]</td>
			</tr>
			<tr>
				<th scope="row">Altitud</th>
				<td>[#startEleString#]</td>
				<td>[#varEleString#]</td>
				<td>[#endEleString#]</td>
			</tr>
		</tbody>
	</table>
	<table class="table table-striped table-bordered table-hover">
		<caption class="caption-top"></caption>
		<thead class="thead-dark ">
			<tr>
				<th></th>
				<th scope="col">Mínimo</th>
				<th scope="col">Promedio</th>
				<th scope="col">Máximo</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">Altitud</th>
				<td>[#minEleString#]</td>
				<td>[#avgEleString#]</td>
				<td>[#maxEleString#]</td>
			</tr>
			<tr>
				<th scope="row">Velocidad</th>
				<td>[#minSpeedString#]</td>
				<td>[#avgSpeedString#]</td>
				<td>[#maxSpeedString#]</td>
			</tr>
			<tr>
				<th scope="row">HDOP</th>
				<td>[#minHDOPString#]</td>
				<td>[#avgHDOPString#]</td>
				<td>[#maxHDOPString#]</td>
			</tr>
		</tbody>
	</table>
</template>

<template id="t4">
	<canvas class="mw-100 max-vh-100 py-3" id="[#id#]"></canvas>
</template>

<!-- <gpx
	version="1.0"
	creator="GPSLogger - http://gpslogger.mendhak.com/"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.topografix.com/GPX/1/0"
	xsi:schemaLocation="http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd"
>
	<time>2019-03-24T22:59:43Z</time>
	<bounds />
	<trk>
		<trkseg>

			<trkpt
				lat="-31.40975743"
				lon="-64.18225199"
			>
				<ele>403.0</ele>
				<hdop>5.4</hdop>
				<src>gps</src>
				<sat>21</sat>
				<time>2019-03-24T22:59:43Z</time>
			</trkpt>

			<trkpt
				lat="-31.40975743"
				lon="-64.18225199"
			>
				<ele>403.0</ele>
				<hdop>5.4</hdop>
				<src>gps</src>
				<sat>21</sat>
				<time>2019-03-24T22:59:43Z</time>
			</trkpt>

		</trkseg>
	</trk>
</gpx> -->

<!-- <kml
	xmlns="http://www.opengis.net/kml/2.2"
	xmlns:gx="http://www.google.com/kml/ext/2.2"
	xmlns:kml="http://www.opengis.net/kml/2.2"
	xmlns:atom="http://www.w3.org/2005/Atom"
>
	<Document>
		<name>2019-03-24T22:59:43Z</name>
		<Placemark>
			<gx:Track>

				<when>2019-03-24T22:59:43Z</when>
				<gx:coord>-64.18225199 -31.40975743 403.0</gx:coord>

				<when>2019-03-24T22:59:43Z</when>
				<gx:coord>-64.18225199 -31.40975743 403.0</gx:coord>

			</gx:Track>
		</Placemark>
	</Document>
</kml> -->

<script defer>

class L10N{
	static TimeUnits(){return {ms:'milisegundos',s:'segundos',m:'minutos',h:'horas',d:'días',s:'semanas',};}
	static LatitudeSigns(){return {'-':'&nbsp;Sur','+':'&nbsp;Norte',};}
	static LongitudeSigns(){return {'-':'&nbsp;Oeste','+':'&nbsp;Este',};}
	static ElevationUnit(){return 'msnm';}
	static LongitudeUnit(){return 'm';}
	static DistanceFactor(){return 0.001;}
	static DistanceUnit(){return 'km';}
	static NA(){return 'no disponible';}
	static SpeedFactor(){return 3600;}
	static SpeedUnit(){return 'km/h';}
	static SpeedLabel(){return 'Velocidad (km/h)';}
	static ElevationLabel(){return 'Altitud (msnm)';}
}

class Point{

	static DecToDMS($dec=0,$signAsPrefix=true,$positive='+',$negative='-'){
		let o='',t;
		t=Math.abs($dec);
		o+=Math.floor(t)+'°';
		t=(t%1)*60;
		o+=Math.floor(t)+'\'';
		t=(t%1)*60;
		o+=Math.roundTo(t,1)+'\'\'';
		if($signAsPrefix){
			o=($dec<0?$negative:$positive)+o;
		}
		else{
			o+=$dec<0?$negative:$positive;
		}
		return o;
	}

	constructor($props={}){
		this.time=$props.time?$props.time:null;
		this.lat =$props.lat ?$props.lat :null;
		this.lon =$props.lon ?$props.lon :null;
		this.ele =$props.ele ?$props.ele :null;
		this.hdop=$props.hdop?$props.hdop:null;
		this.src =$props.src ?$props.src :null;
		this.sat =$props.sat ?$props.sat :null;
		this.duration=0;
		this.distance=0;
	}

	get dateTimeString(){return this.time.getFullDateTime();}
	get timeString(){return this.time.getFullTime();}
	get dateString(){return this.time.getFullDate();}

	get latString(){return Point.DecToDMS(this.lat,false).replace(L10N.LatitudeSigns());}
	get lonString(){return Point.DecToDMS(this.lon,false).replace(L10N.LongitudeSigns());}
	get coordsString(){return this.latString+' '+this.lonString;}
	get eleString(){return this.ele+' '+L10N.ElevationUnit();}

	get speed(){return this.distance&&this.duration?this.distance/this.duration:0;}

}

class Track{

	constructor($type=''){
		this.type=$type;
		this.points=[];
	}

	addPoint($props){return this.points.push(new Point($props));}

	get length(){return this.points.length;}

	get startPoint(){return this.points[0];}
	get endPoint(){return this.points[this.points.length-1];}

	get startDateTimeString(){return this.startPoint.dateTimeString;}
	get endDateTimeString(){return this.endPoint.dateTimeString;}
	get startDateString(){return this.startPoint.dateString;}
	get endDateString(){return this.endPoint.dateString;}
	get startTimeString(){return this.startPoint.timeString;}
	get endTimeString(){return this.endPoint.timeString;}

	get duration(){
		let o=0;
		for(let p=1,a=0;p<this.points.length;p++,a++){
			if(!this.points[p].duration){
				this.points[p].duration=
					this.points[p].time.getTime()
					-
					this.points[a].time.getTime()
				;
			}
			o+=this.points[p].duration;
		}
		return o;
	}
	get durationString(){return this.duration.toTimeFormat(0).replace(L10N.TimeUnits());}
	get durationExactString(){return this.duration.toISOTimeFormat(0);}

	get startCoordsString(){return this.startPoint.coordsString;}
	get endCoordsString(){return this.endPoint.coordsString;}

	get startEleString(){return this.startPoint.eleString;}
	get endEleString(){return this.endPoint.eleString;}
	get varEleString(){return (this.endPoint.ele-this.startPoint.ele)+' '+L10N.LongitudeUnit();}

	get distance(){
		let o=0;
		for(let p=1,a=0,h=Math.PI/180,q=Math.PI/360,t;p<this.points.length;p++,a++){
			if(!this.points[p].distance){
				t=
					Math.sin(
						q * ( this.points[p].lat - this.points[a].lat )
					) ** 2
					+
					Math.sin(
						q * ( this.points[p].lon - this.points[a].lon )
					) ** 2
					*
					Math.cos(
						h * this.points[a].lat
					)
					*
					Math.cos(
						h * this.points[p].lat
					)
				;
				this.points[p].distance=
					12742000
					*
					Math.atan2(
						Math.sqrt(t)
						,Math.sqrt(1-t)
					)
				;
			}
			o+=this.points[p].distance;
		}
		return o;
	}
	get distanceString(){return Math.roundTo(this.distance*L10N.DistanceFactor(),1)+' '+L10N.DistanceUnit();}

	get minEleString(){
		let t=this.points.filter($v=>$v.ele!==null).map($v=>$v.ele);
		return t.length
			?t.min()+' '+L10N.ElevationUnit()
			:L10N.NA()
		;
	}
	get maxEleString(){
		let t=this.points.filter($v=>$v.ele!==null).map($v=>$v.ele);
		return t.length
			?t.max()+' '+L10N.ElevationUnit()
			:L10N.NA()
		;
	}
	get avgEleString(){
		let t=this.points.filter($v=>$v.ele!==null).map($v=>+$v.ele);
		return t.length
			?Math.roundTo(t.avg(),1)+' '+L10N.ElevationUnit()
			:L10N.NA()
		;
	}

	get minSpeedString(){
		this.distance;
		this.duration;
		let t=this.points.filter($v=>$v.speed).map($v=>$v.speed);
		return t.length
			?Math.roundTo(t.min()*L10N.SpeedFactor(),1)+' '+L10N.SpeedUnit()
			:L10N.NA()
		;
	}
	get maxSpeedString(){
		this.distance;
		this.duration;
		let t=this.points.filter($v=>$v.speed).map($v=>$v.speed);
		return t.length
			?Math.roundTo(t.max()*L10N.SpeedFactor(),1)+' '+L10N.SpeedUnit()
			:L10N.NA()
		;
	}
	get avgSpeedString(){
		this.distance;
		this.duration;
		let t=this.points.filter($v=>$v.speed).map($v=>$v.speed);
		return t.length
			?Math.roundTo(t.avg()*L10N.SpeedFactor(),1)+' '+L10N.SpeedUnit()
			:L10N.NA()
		;
	}

	get minHDOPString(){
		let t=this.points.filter($v=>$v.hdop!==null).map($v=>$v.hdop);
		return t.length?Math.round(t.min()):L10N.NA();
	}
	get maxHDOPString(){
		let t=this.points.filter($v=>$v.hdop!==null).map($v=>$v.hdop);
		return t.length?Math.round(t.max()):L10N.NA();
	}
	get avgHDOPString(){
		let t=this.points.filter($v=>$v.hdop!==null).map($v=>+$v.hdop);
		return t.length?Math.round(t.avg()):L10N.NA();
	}

}

function cargar_mapa($div_id,$track){

	let div=document.getElementById($div_id);
	div.map=L.map($div_id);
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
		maxZoom:19,
		attribution:'&copy; <a href="https://www.osm.org/copyright">OpenStreetMap</a> contributors',
	}).addTo(div.map);

	let track=L.polyline.antPath($track.points,{
		delay:900,
		dashArray:[18,36],
		weight:5,
		color:'blue',
		pulseColor:'white',
		paused:false,
		reverse:false,
		hardwareAccelerated:true,
	}).addTo(div.map);

	div.map.fitBounds(track.getBounds());

}

function cargar_grafico($canvas_id,$track){
	let context=document.getElementById($canvas_id).getContext('2d');

	$track.distance;
	$track.duration;

	let da=[$track.points[0].distance];
	let ep=[$track.points[0].ele];
	let vp=[Math.roundTo($track.points[0].speed*L10N.SpeedFactor(),1)];
	for(let i=1,a=0;i<$track.points.length;i++,a++){
		da.push(da[a]+$track.points[i].distance*L10N.DistanceFactor());
		ep.push($track.points[i].ele);
		vp.push(Math.roundTo($track.points[i].speed*L10N.SpeedFactor(),1));
	}

	da=da.map($v=>Math.roundTo($v,1)+' '+L10N.DistanceUnit());

	let chart=new Chart(context,{
		type:'line',
		data:{
			labels:da,
			datasets:[
				{
					label:L10N.ElevationLabel(),
					borderColor:'red',
					backgroundColor:'transparent',
					data:ep,
					yAxisID:'y-axis-1',
				},
				{
					label:L10N.SpeedLabel(),
					borderColor:'blue',
					backgroundColor:'transparent',
					data:vp,
					yAxisID:'y-axis-2'
				},
			]
		},
		options:{
			scales:{
				yAxes:[
					{
						type:'linear',
						display:true,
						position:'left',
						id:'y-axis-1',
					},
					{
						type:'linear',
						display:true,
						position:'right',
						id:'y-axis-2',
					},
				],
			}
		}
	});

}

function procesar($datos){
	let data=(new DOMParser).parseFromString($datos,'text/xml');
	let track=null;
	switch(data.documentElement.tagName){
		case 'kml':{

			track=new Track(data.documentElement.tagName);

			for(
				let i=0
					,l=data.querySelectorAll('kml>Document>Placemark>Track>*')
				;
				i<l.length
				;
				i++
			){
				let t=new Date(l[i].textContent);
				i++;
				let a=l[i].textContent.split(/[^\-0-9.]/).map($v=>+$v);
				track.addPoint({
					time:t,
					lat:a[1],
					lon:a[0],
					ele:a[2],
				});
			}

		}break;
		case 'gpx':{

			track=new Track(data.documentElement.tagName);

			for(
				let i=0
					,l=data.querySelectorAll('gpx>trk>trkseg>trkpt')
					,p
				;
				i<l.length
				;
				i++
			){
				p={
					lat:+l[i].getAttribute('lat'),
					lon:+l[i].getAttribute('lon'),
				};
				if(l[i].querySelector('time'))p.time=new Date(l[i].querySelector('time').textContent);
				if(l[i].querySelector('ele')) p.ele=l[i].querySelector('ele').textContent;
				if(l[i].querySelector('hdop'))p.hdop=l[i].querySelector('hdop').textContent;
				if(l[i].querySelector('src')) p.src=l[i].querySelector('src').textContent;
				if(l[i].querySelector('sat')) p.sat=l[i].querySelector('sat').textContent;
				track.addPoint(p);
			}

		}break;
		default:{
			bs_modal({
				title:'Error',
				close:'Cerrar',
				body:'El archivo seleccionado no es un archivo KLM o GPX valido.',
				focus:false,
			});
		}
	}
	if(track){

		let id_mapa='MAPA_'+Date.now();
		$('#c1').append($('#t1').html().template({id:id_mapa}));
		cargar_mapa(id_mapa,track);

		$('#c3').append($('#t3').html().template(track));

		let id_graf='GRAF_'+Date.now();
		$('#c4').append($('#t4').html().template({id:id_graf}));
		cargar_grafico(id_graf,track);

	}
}

$(document).ready(function(){

	$('#archivo').on('click',function(){
		$('#c1,#c3,#c4').html('');
	});

	$('#archivo').on('change',function(){
		if(
			$('#archivo').get(0)
			&& $('#archivo').get(0).files
			&& $('#archivo').get(0).files[0]
			&& $('#archivo').get(0).files[0] instanceof File
		){
			$('#archivo').toggleProp('disabled');
			let reader=new FileReader();
			reader.onload=function($e){
				procesar($e.target.result);
				$('#archivo').toggleProp('disabled');
			};
			reader.readAsText($('#archivo').get(0).files[0]);
		}
	});


	procesar('<\?xml version="1.0" encoding="UTF-8" ?>\
	<gpx version="1.0" creator="GPSLogger - http://gpslogger.mendhak.com/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.topografix.com/GPX/1/0" xsi:schemaLocation="http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd">\
		<time>2019-02-24T18:49:10Z</time>\
		<bounds />\
		<trk>\
			<trkseg>\
				<trkpt lat="-31.41710447" lon="-64.19319721"><ele>420.0</ele><hdop>19.4</hdop><src>gps</src><sat>18</sat><time>2019-02-24T18:49:10Z</time></trkpt>\
				<trkpt lat="-31.41539186" lon="-64.19862259"><ele>403.0</ele><hdop>12.0</hdop><src>gps</src><sat>17</sat><time>2019-02-24T18:51:45Z</time></trkpt>\
				<trkpt lat="-31.41483917" lon="-64.20028046"><ele>449.0</ele><hdop>11.8</hdop><src>gps</src><sat>21</sat><time>2019-02-24T18:53:41Z</time></trkpt>\
				<trkpt lat="-31.41503207" lon="-64.20017245"><ele>419.0</ele><hdop>10.2</hdop><src>gps</src><sat>22</sat><time>2019-02-24T18:55:02Z</time></trkpt>\
				<trkpt lat="-31.41400242" lon="-64.20219644"><ele>498.0</ele><hdop>15.0</hdop><src>gps</src><sat>23</sat><time>2019-02-24T18:56:48Z</time></trkpt>\
				<trkpt lat="-31.41354224" lon="-64.20219210"><ele>369.0</ele><hdop>24.4</hdop><src>gps</src><sat>21</sat><time>2019-02-24T19:01:00Z</time></trkpt>\
				<trkpt lat="-31.41344771" lon="-64.20318897"><ele>429.0</ele><hdop>11.2</hdop><src>gps</src><sat>23</sat><time>2019-02-24T19:02:19Z</time></trkpt>\
				<trkpt lat="-31.41308819" lon="-64.20475683"><ele>423.0</ele><hdop>8.0</hdop><src>gps</src><sat>21</sat><time>2019-02-24T19:03:34Z</time></trkpt>\
				<trkpt lat="-31.41194828" lon="-64.20829838"><ele>475.0</ele><hdop>7.4</hdop><src>gps</src><sat>21</sat><time>2019-02-24T19:04:56Z</time></trkpt>\
				<trkpt lat="-31.41073517" lon="-64.21142430"><ele>459.0</ele><hdop>7.2</hdop><src>gps</src><sat>21</sat><time>2019-02-24T19:06:22Z</time></trkpt>\
				<trkpt lat="-31.41196085" lon="-64.21309115"><ele>441.0</ele><hdop>8.4</hdop><src>gps</src><sat>22</sat><time>2019-02-24T19:07:39Z</time></trkpt>\
				<trkpt lat="-31.41167352" lon="-64.21411084"><ele>458.0</ele><hdop>4.6</hdop><src>gps</src><sat>21</sat><time>2019-02-24T19:08:48Z</time></trkpt>\
				<trkpt lat="-31.41062137" lon="-64.21681649"><ele>295.0</ele><hdop>11.6</hdop><src>gps</src><sat>21</sat><time>2019-02-24T19:10:15Z</time></trkpt>\
				<trkpt lat="-31.41065973" lon="-64.21738165"><ele>492.0</ele><hdop>5.2</hdop><src>gps</src><sat>22</sat><time>2019-02-24T19:11:30Z</time></trkpt>\
				<trkpt lat="-31.41016508" lon="-64.22047433"><ele>443.0</ele><hdop>8.4</hdop><src>gps</src><sat>22</sat><time>2019-02-24T19:12:47Z</time></trkpt>\
				<trkpt lat="-31.40942026" lon="-64.22402808"><ele>391.0</ele><hdop>10.6</hdop><src>gps</src><sat>22</sat><time>2019-02-24T19:14:19Z</time></trkpt>\
				<trkpt lat="-31.40883040" lon="-64.22504381"><ele>479.0</ele><hdop>7.8</hdop><src>gps</src><sat>22</sat><time>2019-02-24T19:15:38Z</time></trkpt>\
				<trkpt lat="-31.40807581" lon="-64.22778301"><ele>484.0</ele><hdop>9.4</hdop><src>gps</src><sat>23</sat><time>2019-02-24T19:17:03Z</time></trkpt>\
				<trkpt lat="-31.40771137" lon="-64.22963847"><ele>474.0</ele><hdop>3.8</hdop><src>gps</src><sat>21</sat><time>2019-02-24T19:18:29Z</time></trkpt>\
				<trkpt lat="-31.40804495" lon="-64.22956243"><ele>441.0</ele><hdop>9.8</hdop><src>gps</src><sat>22</sat><time>2019-02-24T19:20:06Z</time></trkpt>\
				<trkpt lat="-31.40783017" lon="-64.22969527"><ele>476.0</ele><hdop>3.2</hdop><src>gps</src><sat>21</sat><time>2019-02-24T19:21:40Z</time></trkpt>\
				<trkpt lat="-31.40783529" lon="-64.22969587"><ele>479.0</ele><hdop>9.0</hdop><src>gps</src><sat>21</sat><time>2019-02-24T19:22:59Z</time></trkpt>\
				<trkpt lat="-31.40782795" lon="-64.22970113"><ele>481.0</ele><hdop>11.0</hdop><src>gps</src><sat>21</sat><time>2019-02-24T19:24:38Z</time></trkpt>\
				<trkpt lat="-31.40777819" lon="-64.23212933"><ele>475.0</ele><hdop>9.6</hdop><src>gps</src><sat>22</sat><time>2019-02-24T19:26:00Z</time></trkpt>\
				<trkpt lat="-31.40769101" lon="-64.23566901"><ele>490.0</ele><hdop>7.6</hdop><src>gps</src><sat>23</sat><time>2019-02-24T19:27:49Z</time></trkpt>\
			</trkseg>\
		</trk>\
	</gpx>');

});

</script>
