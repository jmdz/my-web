<?

require('../../apps/common.inc.php');

{$webpage
	->copy_year(2021)
	->title_segs('Husos horarios')
	->desc('Convierte fechas y horas de un huso horario a otro.')
	->keyw('huso horario, timezone, tz')
;}

$webpage->head();

?>
<style>
</style>
<? $webpage->body() ?>

<div class="row">
	<div class="col-12 col-sm-12 col-md-6 mt-3">
		<p>Los <a href="https://es.wikipedia.org/wiki/Huso_horario">husos horarios</a> (<a href="https://en.wikipedia.org/wiki/Time_zone">time zones</a> en ingles) son las áreas en las que se divide la tierra en las que por convención rige el mismo horario.</p>

		<p>Se definen en relación al denominado <strong>tiempo universal coordinado</strong> (<strong>UTC</strong>, el lio que organizaron los angloparlantes que querían <em>CUT</em> y los francófonos que querían <em>TUC</em> es la razón por la que esta abreviatura no tiene gollete), por ejemplo la hora oficial argentina esta definida como UTC-03:00.</p>

		<p class="text-center">
			<a href="https://en.wikipedia.org/wiki/File:World_Time_Zones_Map.png">
				<img
					class="img-fluid"
					src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/World_Time_Zones_Map.png/1280px-World_Time_Zones_Map.png"
					alt="Mapa de husos horarios (fuente: Wikipedia)"
				/>

				Mapa de husos horarios (fuente: Wikipedia)
			</a>
		</p>
	</div>

	<div class="col-12 col-sm-12 col-md-6 mt-3">
		<fieldset class="form-group">
			<legend class="">Convertir esta fecha y hora del huso</legend>

			<label for="e_huso">Huso:</label>
			<select class="form-control" id="e_huso"></select>

			<label for="e_fecha">Fecha:</label>
			<input class="form-control" type="date" id="e_fecha" />

			<label for="e_hora">Hora:</label>
			<input class="form-control" type="time" id="e_hora" />
		</fieldset>

		<fieldset class="form-group">
			<legend class="">Al huso</legend>

			<label for="r_huso">Huso:</label>
			<select class="form-control" id="r_huso"></select>

			<label for="r_fecha">Fecha:</label>
			<output class="form-control" type="date" id="r_fecha"></output>

			<label for="r_hora">Hora:</label>
			<output class="form-control" type="time" id="r_hora"></output>
		</fieldset>
	</div>
</div>

<script defer>

function calcular(){
	let e_fecha = $('#e_fecha').val();
	let e_hora = $('#e_hora').val();
	let e_huso = $('#e_huso').val();
	let r_huso = $('#r_huso').val();

	let r = (new Date(`${e_fecha} ${e_hora}`)).addMilliseconds(
		(new Date(`${e_fecha} ${e_hora}${e_huso}`)).getTime()
		-
		(new Date(`${e_fecha} ${e_hora}${r_huso}`)).getTime()
	);

	$('#r_fecha').val(r.toLocaleDateString());
	$('#r_hora').val(r.toLocaleTimeString());
}

$(document).ready(function(){
	let ahora = new Date;

	$('#e_fecha').val(ahora.getFullDate());
	$('#r_fecha').val(ahora.getFullDate());

	$('#e_hora').val(ahora.getFullTime());
	$('#r_hora').val(ahora.getFullTime());

	$.each(Date.timeZones, (k, v) => {
		$('#e_huso').append(`<option value="${v}" data-timezone="${k}">UTC${v} (${k})</option>`);
		$('#r_huso').append(`<option value="${v}" data-timezone="${k}">UTC${v} (${k})</option>`);
	});

	let ltz = ahora.getTimezones();
	if(ltz._) {
		$(`#e_huso option[data-timezone="${ltz._}"]`).prop('selected', true);
		$(`#r_huso option[data-timezone="${ltz._}"]`).prop('selected', true);
		$(`#e_huso option[value="/LOCAL"]`).attr('value', ltz[ltz._]);
		$(`#r_huso option[value="/LOCAL"]`).attr('value', ltz[ltz._]);
	} else {
		$(`#e_huso option[data-timezone="Z"]`).prop('selected', true);
		$(`#r_huso option[data-timezone="Z"]`).prop('selected', true);
		$(`#e_huso option[value="/LOCAL"]`).prop('disabled', true);
		$(`#r_huso option[value="/LOCAL"]`).prop('disabled', true);
	}

	$('#e_fecha,#e_hora,#e_huso,#r_huso').on('change', calcular);
});

</script>
