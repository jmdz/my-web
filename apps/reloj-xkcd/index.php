<?php

require('../../apps/common.inc.php');

{$webpage
	->copy_year(2019)
	->title_segs('Reloj XKCD')
	->desc('Reloj XKCD, mapamundi proyección polar.')
	->keyw('reloj, xkcd, mapamundi, proyección, polar')
;}

$webpage->body();

?>

<div class="row">
	<div class="col-12 col-sm-12 col-md-3">
		<p>Hace algunos años <a href="https://xkcd.com/">xkcd</a> publicó este <a href="https://xkcd.com/1335">genial "reloj"</a> que además se mantiene más o menos sincronizado. Esto último fue lo que me llamo la atención, ¿cómo lo hace? después de revisarles el código me di cuenta, que en el servidor corre algún programa que le indica al navegador que imagen cargar según la hora, o sea, tienen una montón de imágenes.</p>
		<p>Pero estamos en el 2019 así que no hace falta hacer eso, podemos cargar una sola imagen y hacer que el navegador la gire para acomodarla, incluso en tiempo real.</p>
	</div>
	<div class="col-12 col-sm-12 col-md-9" style="padding-bottom: 20vh;">
		<div class="text-center">
			<img
				id="xkcd_now"
				class="mw-100 max-vh-100"
				src="xkcd-now.png"
				data-map-src="xkcd-now-map.png"
				data-ref-src="xkcd-now-ref.png"
			/>
		</div>
	</div>
</div>

<script defer>

function xkcd_now_update($img_selector){
	if(!xkcd_now_update._img&&$img_selector){
		xkcd_now_update._img=$($img_selector);
		xkcd_now_update._img
			.attr(
				'data-orig-src'
				,xkcd_now_update._img.attr('src')
			)
			.attr(
				'src'
				,xkcd_now_update._img.attr('data-map-src')
			)
			.parent()
				.css({
					'background-size':'contain',
					'background-repeat':'no-repeat',
					'background-position':'center',
					'background-image':'url('
						+xkcd_now_update._img.attr('data-ref-src')
						+')',
				})
		;
	}
	if(xkcd_now_update._img){
		let now=new Date();
		let deg=(now.getUTCHours()*60+now.getUTCMinutes())*0.25-180;
		xkcd_now_update._img.css({
			'transform':'rotate('+deg+'deg)',
		});
		setTimeout(xkcd_now_update,60000);
	}
}

$(document).ready(function(){
	xkcd_now_update('#xkcd_now');
});

</script>
