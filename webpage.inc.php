<?php

error_reporting(-1);

final class webpage{

	public static function create(){return new self;}

	private $cwd;

	private function __construct(){$this->cwd=getcwd();}

	public function cwd(){return $this->cwd;}

	private $domains=[];
	public function add_domain($domain,$stat,$path,$libs,$prod){
		$this->domains[$domain]=(object)[
			'path'=>$path
			,'stat'=>$stat
			,'libs'=>$libs
			,'prod'=>$prod
		];
		return $this;
	}

	private function iset($prop,$val,$key=null){
		if(is_null($val)){
			$t=$this->$prop;
			return is_null($key)?$t:$t[$key];
		}
		if(!is_array($this->$prop))
			$this->$prop=$val;
		elseif(is_null($key))
			$this->{$prop}[]=$val;
		else
			$this->{$prop}[$key]=$val;
		return $this;
	}

	private $lang='en';             public function lang(        $val=null            ){return $this->iset('lang'         ,$val        );}
	private $lang_dir='ltr';        public function lang_dir(    $val=null            ){return $this->iset('lang_dir'     ,$val        );}
	private $mime_type='text/html'; public function mime_type(   $val=null            ){return $this->iset('mime_type'    ,$val        );}
	private $charset='UTF-8';       public function charset(     $val=null            ){return $this->iset('charset'      ,$val        );}
	private $title_site;            public function title_site(  $val=null            ){return $this->iset('title_site'   ,$val        );}
	private $title_glue=' / ';      public function title_glue(  $val=null            ){return $this->iset('title_glue'   ,$val        );}
	private $title_segs=[];         public function title_segs(  $val=null            ){return $this->iset('title_segs'   ,$val        );}
	private $author;                public function author(      $val=null            ){return $this->iset('author'       ,$val        );}
	private $copy_owner;            public function copy_owner(  $val=null            ){return $this->iset('copy_owner'   ,$val        );}
	private $copy_year;             public function copy_year(   $val=null            ){return $this->iset('copy_year'    ,$val        );}
	private $desc;                  public function desc(        $val=null            ){return $this->iset('desc'         ,$val        );}
	private $keyw;                  public function keyw(        $val=null            ){return $this->iset('keyw'         ,$val        );}
	private $styles=[];             public function style(       $attrs=null          ){return $this->iset('styles'       ,$attrs      );}
	private $scripts=[];            public function script(      $attrs=null          ){return $this->iset('scripts'      ,$attrs      );}
	private $html_attrs=[];         public function html_attrs(  $name=null ,$val=null){return $this->iset('html_attrs'   ,$val  ,$name);}
	private $body_attrs=[];         public function body_attrs(  $name=null ,$val=null){return $this->iset('body_attrs'   ,$val  ,$name);}
	private $inc_on_body=[];        public function inc_on_body( $val=null            ){return $this->iset('inc_on_body'  ,$val        );}
	private $inc_on_end=[];         public function inc_on_end(  $val=null            ){return $this->iset('inc_on_end'   ,$val        );}
	private $headers=[];			public function http_head(   $kv=null             ){return $this->iset('headers'      ,$kv         );}

	private $vars=[];
	public function set_var($name,$val=null){$this->vars[$name]=$val;return $this;}
	public function del_var($name){unset($this->vars[$name]);return $this;}
	public function has_var($name){return isset($this->vars[$name]);}
	public function get_var($name,$defval=null){return $this->has_var($name)?$this->vars[$name]:$defval;}

	private $inline_styles=[];
	public function inline_style($rules=[],$attrs=[]){
		return $this->iset(
			'inline_styles'
			,(
				$rules||$attrs
				?(compact('rules','attrs'))
				:null
			)
		);
	}

	private $reg_pkg=[];
	public function reg_pkg($name=null,$scr=[],$sty=[]){
		return $this->iset(
			'reg_pkg'
			,(
				$scr||$sty
				?([$scr,$sty])
				:null
			)
			,$name
		);
	}

	public function inc_pkg($name){
		if($this->reg_pkg[$name]){
			array_walk(
				$this->reg_pkg[$name][0]
				,array($this,'script')
			);
			array_walk(
				$this->reg_pkg[$name][1]
				,array($this,'style')
			);
		}
		return $this;
	}

	public function prod(){
		return $this->domains[$_SERVER['HTTP_HOST']]->prod;
	}

	public function stat($s=''){
		return $this->domains[$_SERVER['HTTP_HOST']]->stat.''.$s;
	}

	public function root($s='',$d=false,$p=false){
		$p=$p?'http:':'';
		return ($d?"$p//{$_SERVER['HTTP_HOST']}":'').$this->domains[$_SERVER['HTTP_HOST']]->path.''.$s;
	}

	public function inc_lib($path,$fatal=true){
		if($fatal)
			require($this->domains[$_SERVER['HTTP_HOST']]->libs.''.$path);
		else
			include($this->domains[$_SERVER['HTTP_HOST']]->libs.''.$path);
		return $this;
	}

	public function title($full=false){
		return ($full?$this->title_site:'').($this->title_segs?($full?$this->title_glue:'').implode($this->title_glue,$this->title_segs):'');
	}

	public function copy(){
		return '© '.$this->copy_year.($this->copy_year<date('Y')?date('‑Y'):'').', '.$this->copy_owner;
	}

	private static function attrs($attrs=array()){
		return implode(''
			,array_map(
				function($v,$n){return is_numeric($n)?" $v":" $n=\"$v\"";}
				,$attrs
				,array_keys($attrs)
			)
		);
	}

	private static function css_rules($rules = [], $tabs = 0) {
		$r = '';
		$t = str_repeat("\t", $tabs);
		foreach ($rules as $k => $v) {
			if (is_numeric($k)) {
				$r .= $t . $v . ';' . PHP_EOL;
			} else {
				if (is_array($v)) {
					$r .= $t . $k . ' {' . PHP_EOL;
					$r .= self::css_rules($v, $tabs + 1);
					$r .= $t . '}' . PHP_EOL;
				} else {
					$r .= $t . $k . ': ' . $v . ';' . PHP_EOL;
				}
			}
		}
		return $r;
	}

	private $started=false;
	public function head($html_attrs=[]){
		$html_attrs=array_merge($this->html_attrs,$html_attrs);
		if(!isset($this->domains[$_SERVER['HTTP_HOST']])){
			header('Location: //'.key($this->domains));
			exit();
		}
		$this->started=true;
		header("Content-Type: {$this->mime_type}; charset={$this->charset}");
		array_walk($this->headers,'header');
		if($this->mime_type==='text/html'){
			echo ''
				,'<!DOCTYPE html>',PHP_EOL
				,'<html',self::attrs($html_attrs),' lang="',$this->lang,'" dir="',$this->lang_dir,'">',PHP_EOL
				,'<head>',PHP_EOL
				,'<meta charset="',strtolower($this->charset),'"/>',PHP_EOL
				,'<meta http-equiv="Content-Type" content="',$this->mime_type,'; charset=',$this->charset,'"/>',PHP_EOL
				,'<meta http-equiv="Content-Script-Type" content="application/javascript"/>',PHP_EOL
				,'<meta http-equiv="Content-Style-Type" content="text/css"/>',PHP_EOL
				,'<meta http-equiv="Content-Languaje" content="',$this->lang,'"/>',PHP_EOL
				,'<meta name="viewport" content="width=device-width,initial-scale=1"/>',PHP_EOL
				,'<meta name="language" content="',$this->lang,'"/>',PHP_EOL
				,'<link rel="icon" type="image/x-icon" sizes="16x16 32x32 48x48" href="',$this->stat('/favicon.ico'),'"/>',PHP_EOL
				,'<link rel="apple-touch-icon-precomposed" type="image/png" sizes="152x152" href="',$this->stat('/apple-touch-icon-precomposed.png'),'"/>',PHP_EOL
				,'<meta name="msapplication-TileImage" type="image/png" sizes="144x144" content="',$this->stat('/favicon-144.png'),'"/>',PHP_EOL
				,'<title>',$this->title(true),'</title>',PHP_EOL
				,'<meta name="author" content="',$this->author,'"/>',PHP_EOL
				,'<meta name="copyright" content="',$this->copy(),'"/>',PHP_EOL
				,'<meta name="description" content="',$this->desc,'"/>',PHP_EOL
				,'<meta name="keywords" content="',$this->keyw,'"/>',PHP_EOL
				,implode('',array_map(function($e){
					$e=array_merge(['rel'=>'stylesheet'],$e);
					$e['href']=preg_match('~^(https?:)?//~',$e['href'])?$e['href']:$this->stat($e['href']);
					return '<link'.self::attrs($e).'/>'.PHP_EOL;
				},$this->styles))
				,implode('',array_map(function($e){
					return '<style'.self::attrs($e['attrs']).'>'.PHP_EOL.self::css_rules($e['rules']).'</style>'.PHP_EOL;
				},$this->inline_styles))
			;
		}
	}

	public function body($body_attrs=[]){
		$body_attrs=array_merge($this->body_attrs,$body_attrs);
		if(!$this->started){
			$this->head();
		}
		if($this->mime_type==='text/html'){
			echo ''
				,implode('',array_map(function($e){
					$e['src']=preg_match('~^(https?:)?//~',$e['src'])?$e['src']:$this->stat($e['src']);
					return '<script'.self::attrs($e).'></script>'.PHP_EOL;
				},$this->scripts))
			;
			echo '</head>',PHP_EOL;
			flush();
			echo '<body',self::attrs($body_attrs),'>';
			echo PHP_EOL;
			array_walk(
				$this->inc_on_body
				,function($v){
					$webpage=$this;
					include($this->cwd().'/'.$v);
				}
			);
		}
		else{
			flush();
		}
	}

	public function __destruct(){
		if($this->started&&$this->mime_type==='text/html'){
			array_walk(
				$this->inc_on_end
				,function($v){
					$webpage=$this;
					include($this->cwd().'/'.$v);
				}
			);
			echo ''
				,PHP_EOL
				,'</body>'
				,PHP_EOL
				,'</html>'
			;
		}
	}

	public function json($value,$options=0){
		$this->mime_type('application/json');
		$this->charset('UTF-8');
		$this->head();
		echo json_encode($value,$options);
		exit();
	}

	public function redirect($url,$code=302){
		header('Location: '.$url, true, $code);
		exit();
	}
}

$localhost = substr($_SERVER['SERVER_NAME'], 0, 8) === '192.168.' ? $_SERVER['SERVER_NAME'] : 'localhost';

{$webpage=webpage::create()

	//add_domain($domain          ,$stat                                   ,$path             ,$libs                                         ,$prod)

	->add_domain('jmdz.com.ar'    ,'//static.jmdz.com.ar/'                 ,'/'               ,'/home/www/static.jmdz.com.ar/libs/'          ,true )
	->add_domain('www.jmdz.com.ar','//static.jmdz.com.ar/'                 ,'/'               ,'/home/www/static.jmdz.com.ar/libs/'          ,true )

	->add_domain('jmdz.ar'        ,'//static.jmdz.com.ar/'                 ,'/'               ,'/home/www/static.jmdz.com.ar/libs/'          ,true )
	->add_domain('www.jmdz.ar'    ,'//static.jmdz.com.ar/'                 ,'/'               ,'/home/www/static.jmdz.com.ar/libs/'          ,true )

	->add_domain($localhost       ,"//$localhost/fh/static.jmdz.com.ar/"   ,'/fh/jmdz.com.ar/','/home/jmdz/Webs/.fh/static.jmdz.com.ar/libs/',false)
	->lang('es-AR')
	->title_site('jMdZ')
	->author('Mauro Daino (jMdZ)')
	->copy_owner('Mauro Daino')
	->copy_year(2011)

	// pkg

	// all (no-home)
	->reg_pkg('jmdz-fix-js'
		,[
			['src'=>'libs/jmdz-fix-js.js']
		]
		,[
		]
	)

	// all (no-home)
	->reg_pkg('jquery-3.4.1'
		,[
			['src'=>'libs/jquery-3.4.1-jquery.min.js']
			,['src'=>'libs/jmdz-fix-jq.js','async']
		]
		,[
		]
	)

	// all (no-home)
	->reg_pkg('popper-1.14.7'
		,[
			['src'=>'libs/popper-1.14.7-popper.min.js','defer']
		]
		,[
		]
	)

	// all (no-home)
	->reg_pkg('bootstrap-4.3.1'
		,[
			['src'=>'libs/bootstrap-4.3.1-bootstrap.min.js','defer']
			,['src'=>'libs/jmdz-fix-bs.js','defer']
		]
		,[
			['href'=>'libs/bootstrap-4.3.1-bootstrap.min.css']
			,['href'=>'libs/jmdz-fix-bs.css']
		]
	)

	// datos-exif
	->reg_pkg('jmdz-js-metadata-jpeg'
		,[
			['src'=>'libs/jmdz-js-metadata-jpeg.js','defer']
			,['src'=>'libs/jmdz-js-metadata-jpeg-es.js','defer']
		]
		,[
		]
	)

	// datos-exif recorido-gps
	->reg_pkg('leaflet-1.5.1'
		,[
			['src'=>'libs/leaflet-1.5.1/leaflet.js','defer']
			,['src'=>'libs/Leaflet.Icon.Glyph-0.2.1.js','defer']
			,['src'=>'libs/leaflet-ant-path-1.3.0.js','defer']
		]
		,[
			['href'=>'libs/leaflet-1.5.1/leaflet.css']
		]
	)

	// recorido-gps
	->reg_pkg('chart.js-2.8.0'
		,[
			['src'=>'libs/Chart.js-2.8.0-Chart.min.js','defer']
		]
		,[
		]
	)

	// datos-exif
	->reg_pkg('fontawesome-free-5.8.2'
		,[
		]
		,[
			['href'=>'libs/fontawesome-free-5.8.2/css/all.min.css']
		]
	)

	// zonas-de-adams/
	->reg_pkg('jquery-ui-1.12.1'
		,[
			['src'=>'libs/jquery-ui-1.12.1-jquery-ui.min.js','defer']
		]
		,[
			['href'=>'libs/jquery-ui-1.12.1-jquery-ui.min.css']
		]
	)

	// zonas-de-adams/
	->reg_pkg('jquery.ui.touch-punch-0.2.3'
		,[
			['src'=>'libs/jquery.ui.touch-punch-0.2.3-jquery.ui.touch-punch.min.js','defer']
		]
		,[
		]
	)

;}
